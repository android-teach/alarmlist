package com.ictu.nvq.alarmlist;

import java.util.Calendar;

public class AlarmItem {
    private int hour;
    private int minute;

    public AlarmItem() {
    }

    public AlarmItem(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
    }

    public int getHour() {
        return hour;
    }

    public AlarmItem setHour(int hour) {
        this.hour = hour;
        return this;
    }

    public int getMinute() {
        return minute;
    }

    public AlarmItem setMinute(int minute) {
        this.minute = minute;
        return this;
    }
}
