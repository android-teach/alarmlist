package com.ictu.nvq.alarmlist;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView lvAlarm;
    private List<AlarmItem> alarmList = new ArrayList<>();
    private ArrayAdapter<AlarmItem> adapter;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.context = this;
        bindView();
    }

    private void bindView() {
        lvAlarm = findViewById(R.id.lvAlarm);
        createAdapter();
        lvAlarm.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_add) {
            createAlarm();
        }
        return super.onOptionsItemSelected(item);
    }

    private void createAlarm() {
        Intent i = new Intent(this, AddAlarmActivity.class);
        startActivityForResult(i, 10);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10 && resultCode == RESULT_OK) {
            int hour = data.getIntExtra("hour", 0);
            int minute = data.getIntExtra("minute", 0);
            alarmList.add(new AlarmItem(hour, minute));
            changeList();
        }
    }

    private void createAdapter() {
        adapter = new ArrayAdapter<AlarmItem>(this, 0, alarmList) {
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.item_alarm, parent, false);

                TextView tvTime = convertView.findViewById(R.id.tvTime);

                AlarmItem alarmItem = alarmList.get(position);
                tvTime.setText(alarmItem.getHour() + ":" + alarmItem.getMinute());

                convertView.findViewById(R.id.ivDelete)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                delete(position);
                            }
                        });

                return convertView;
            }
        };
    }

    private void delete(int pos) {
        alarmList.remove(pos);
        changeList();
    }

    private void changeList(){
        adapter.notifyDataSetChanged();
        setTitle("Hẹn giờ (" + alarmList.size() + ")");
    }
}
